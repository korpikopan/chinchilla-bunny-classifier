# chinchilla-bunny-classifier

a small tensorflow classifier that recognizes a chinchilla or a rabbit if a picture is given

# 1 - How to use :

-download the repository or ( git clone it) and make sure you have tensorflow and python installed.
( run "pip install tensorflow" to install tensorflow)
- open a command line and type "python label_image.py <your_picture>.jpg"


